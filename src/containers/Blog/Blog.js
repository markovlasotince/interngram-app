import React, { Component } from "react";
import axios from "../../axios";

import styles from "./Blog.css";
import Posts from "./Posts/Posts";

class Blog extends Component {
  addPost = () => {
    axios.post("/posts/", {
      type: "VIDEO",
      title: "Web Dev Tutorial for Beginners",
      meta: {
        url: "https://www.youtube.com/watch?v=3JluqTojuME"
      }
    });
    window.location.reload();
  };

  render() {
    return (
      <div className={styles.Blog}>
        <header>
          <nav>
            <ul>
              <li>Posts</li>
              <li>
                <button onClick={this.addPost}>Add Post</button>{" "}
              </li>
            </ul>
          </nav>
        </header>
        <Posts />
      </div>
    );
  }
}

export default Blog;
