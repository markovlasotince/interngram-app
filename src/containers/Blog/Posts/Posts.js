import React, { PureComponent } from "react";
import axios from "../../../axios";

import Post from "../../../components/Post/Post";
import ImagePost from "../../../components/Post/ImagePost/ImagePost";
import VideoPost from "../../../components/Post/VideoPost/VideoPost";
import styles from "./Posts.css";

class Posts extends PureComponent {
  state = {
    posts: [],
    limit: 10,
    nextPosts: 10,
    loading: false,
    numOfPosts: 10
  };

  loadUsers = url => {
    axios.get(url).then(response => {
      const posts = response.data;
      const allPosts = [...this.state.posts];
      this.setState({ posts: allPosts.concat(posts) });
    });
  };

  getPostNum = url => {
    axios.get(url).then(response => {
      this.setState({ numOfPosts: response.data.length });
      console.log(response.data.length);
    });
  };

  componentWillMount() {
    this.getPostNum("/posts");
    this.loadUsers("/posts?_limit=" + this.state.limit);
  }

  componentWillUpdate() {
    window.onscroll = () => {
      this.setState({ loading: true });
      if (
        window.innerHeight + document.documentElement.scrollTop ===
          document.documentElement.offsetHeight &&
        this.state.numOfPosts >= this.state.nextPosts
      ) {
        this.loadUsers(
          `http://localhost:3000/posts?_start=${
            this.state.nextPosts
          }&_end=${10 + this.state.nextPosts}`
        );
        const next = this.state.limit + this.state.nextPosts;
        this.setState({
          nextPosts: next
        });
      }
    };
  }

  postDeletedHandler = () => {
    this.setState({ posts: [...this.state.posts] });
  };

  render() {
    console.log("[POSTS.js] Inside render");
    const loading = this.state.loading;
    let posts = this.state.posts.map(post => {
      switch (post.type) {
        case "IMAGE":
          return (
            <ImagePost
              key={post.id + 10}
              id={post.id}
              title={post.title}
              type={post.type}
              meta={post.meta}
              deleted={this.postDeletedHandler}
            />
          );
        case "VIDEO":
          return (
            <VideoPost
              key={post.id + 10}
              id={post.id}
              title={post.title}
              type={post.type}
              meta={post.meta}
              deleted={this.postDeletedHandler}
            />
          );

        default:
          return (
            <Post
              key={post.id + 10}
              id={post.id}
              title={post.title}
              type={post.type}
              deleted={this.postDeletedHandler}
            />
          );
      }
    });
    return (
      <div className={styles.Posts}>
        {posts}
        {loading === true ? (
          <i className="fas fa-spinner fa-pulse" style={{ fontSize: "60px" }} />
        ) : null}
      </div>
    );
  }
}

export default Posts;
