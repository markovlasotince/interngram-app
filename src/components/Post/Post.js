import React, { Component } from "react";
import axios from "../../axios";
import styles from "./Post.css";
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';

import "bootstrap/dist/css/bootstrap.css";

class Post extends Component {
  updatePost = () => {
    axios.put("/posts/1", {
      type: "VIDEO",
      title: "Web Dev Tutorial for Beginners",
      meta: {
        url: "https://www.youtube.com/watch?v=3JluqTojuME"
      }
    });
  };

  deletePost = id => {
    window.location.reload();
    axios.delete(`/posts/${id}`);
  };

  render() {
    return (
      <article className={styles.Post}>
  <ButtonToolbar>
    {['up', 'down', 'left', 'right'].map(direction => (
      <DropdownButton
        drop={direction}
        variant="secondary"
        title={` Drop ${direction} `}
        id={`dropdown-button-drop-${direction}`}
        key={direction}
      >
        <Dropdown.Item eventKey="1">Action</Dropdown.Item>
        <Dropdown.Item eventKey="2">Another action</Dropdown.Item>
        <Dropdown.Item eventKey="3">Something else here</Dropdown.Item>
        <Dropdown.Divider />
        <Dropdown.Item eventKey="4">Separated link</Dropdown.Item>
      </DropdownButton>
    ))}
  </ButtonToolbar>
        <h1>
          {this.props.id}. {this.props.title}
        </h1>
        <div>
          <p>This is post body. {this.props.type}</p>
        </div>
        <button onClick={this.updatePost}>Update Post</button>
        <button onClick={() => this.deletePost(this.props.id)}>
          Delete Post
        </button>
      </article>
    );
  }
}

export default Post;
