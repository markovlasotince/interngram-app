import React from "react";
import Post from "../Post";

import styles from "../Post.css";
import './VideoPost.css'

class VideoPost extends Post {
  videoId = (url) => {
    // const match = url.match(
    //   /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/
    // );
    // return match && match[7].length === 11 ? match[7] : false;
    const  match = url.match(/v=([0-9a-z_-]{1,20})/i);
    return (match ? match['1'] : false);
  };

  render() {
    let id = this.videoId(this.props.meta.url);
    return (
      <div className={styles.Post}>
        <h3>{this.props.title}</h3>
        {/* <iframe 
            title={this.props.title}
            width="560" 
            height="315" 
            src={`https://www.youtube.com/embed/${id}`}
            frameBorder="0" 
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
            allowFullscreen>
        </iframe> */}
        <iframe
          id="ytplayer"
          type="text/html"
          width="560"
          height="315"
          title={this.props.title}
          src={`https://www.youtube.com/embed/${id}`}
          frameBorder="0"
          allowFullScreen
        />
      </div>
    );
  }
}

export default VideoPost;
