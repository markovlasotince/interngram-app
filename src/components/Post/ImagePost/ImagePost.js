import React from "react";
import Post from "../Post";
import ModalImage from "react-modal-image";

import styles from "../Post.css";
import strylesImage from "./ImagePost.css";

class ImagePost extends Post {
  state = {
    lightboxIsOpen: false
  };

  render() {
    return (
      <div className={styles.Post}>
        <h3>{this.props.title}</h3>
        <ModalImage
          className={strylesImage.smallImg}
          small={this.props.meta.url}
          medium={this.props.meta.url}
          alt={this.props.meta.alt}
          hideDownload="true"
          hideZoom="true"
        />
      </div>
    );
  }
}

// Code for regular img tags (no ModalImage)
//let meta = "";
/* return (
  {this.props.meta.alt === undefined || this.props.meta.alt === null
          ? (meta = "")
          : (meta = this.props.meta.alt)}
        <img src={this.props.meta.url} alt={meta} width="1200" height="628" />
  ) */

export default ImagePost;
